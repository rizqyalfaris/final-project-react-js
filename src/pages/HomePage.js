import React, { useContext, useEffect } from "react";
import { Helmet } from "react-helmet";
import Navbar from "../components/Navbar";
import Product from "../components/Product";
import { GlobalContext } from "../context/GlobalContext";
import Footer from "../components/Footer";
import Layout from "../components/layout/Layout";
import { Carousel } from "antd";

function HomePage() {
  const { product, fetchProducts, editProduct, setEditProduct, loading } =
    useContext(GlobalContext);

  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <div>
      <Helmet>
        <title>Home Page</title>
      </Helmet>
      <Layout>
      <Carousel autoplay>
        <div>
          <img className="w-full h-[22rem]" src="https://wallup.net/wp-content/uploads/2017/11/17/223621-leather-black_background.jpg"></img>
        </div>
        <div>
        <img className="w-full h-[22rem]" src="https://i.pinimg.com/originals/23/5c/d1/235cd179e3eec4cd5efb74e2a887b292.jpg"></img>
        </div>
      </Carousel>
        <div className="max-w-5xl mx-auto my-10">
          <h1 class="text-cyan-500 text-3xl px-12">Catalog Products</h1>
          {product.map((product) => (
            <Product key={product.id} product={product} />
          ))}
        </div>
      </Layout>
    </div>
  );
}

export default HomePage;
