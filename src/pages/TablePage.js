import React, { useContext, useEffect } from "react";
import Navbar from "../components/Navbar";
import Product from "../components/Product";
import { GlobalContext } from "../context/GlobalContext";
import Table from "../components/Table";
import Footer from "../components/Footer";
import Layout from "../components/layout/Layout";
import { Helmet } from "react-helmet";

function TablePage() {
    const { product, fetchProducts, editProduct, setEditProduct, loading } = useContext(GlobalContext);

    useEffect(() => {
        fetchProducts();
      }, []);

  return (
    <div>
    <Helmet>
      <title>Table Product Page</title>
    </Helmet>
      <Layout>
      <Table />
      </Layout>
    </div>
  );
}

export default TablePage;
