
import React from 'react'
import WrapperComponent from '../components/WrapperComponent'

function ChildrenPage() {
  return (
    <div>
        <WrapperComponent>
            <div className='my-6 p-6 bg-green-600'>
                <h1 className='text-white text-center text-3xl'>
                    Ini tulisan saat pemanggilan Wrapper component pertama
                </h1>
            </div>
        </WrapperComponent>
    </div>
  )
}

export default ChildrenPage