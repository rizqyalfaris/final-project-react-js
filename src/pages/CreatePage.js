import React, { useContext, useEffect } from "react";
import Navbar from "../components/Navbar";
import Product from "../components/Product";
import { GlobalContext } from "../context/GlobalContext";
import Table from "../components/Table";
import CreateForm from "../components/CreateForm";
import Footer from "../components/Footer";
import Layout from "../components/layout/Layout";
import { Helmet } from "react-helmet";

function CreatePage() {

  return (
    <div>
    <Helmet>
      <title>Create Product Page</title>
    </Helmet>
    <Layout>
      <CreateForm />
    </Layout>
    </div>
  );
}

export default CreatePage;
