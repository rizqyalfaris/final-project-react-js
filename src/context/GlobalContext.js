import axios from "axios";
import { createContext, useState } from "react";

export const GlobalContext = createContext();

export function ContextProvider({ children }) {
  const [product, setProducts] = useState([]);
  const [editProduct, setEditProduct] = useState(null);
  const [loading, setLoading] = useState(false);

  const fetchProducts = async () => {
    // setLoading(true);
    try {
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/products"
      );
      // console.log(response.data.data); //berhasil mengakses data products
      setProducts(response.data.data); //berhasil menyimpan data products pada state
      //products sudah terisi data dari server
    } catch (error) {
      alert("Terjadi sesuatu error!");
    } finally {
      setLoading(false);
    }
  };

  return (
    <GlobalContext.Provider
      value={{
        product: product,
        setProducts: setProducts,
        fetchProducts: fetchProducts,
        editProduct: editProduct,
        setEditProduct: setEditProduct,
        loading: loading,
        setLoading: setLoading,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
}
