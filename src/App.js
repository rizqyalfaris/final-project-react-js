import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom"
import HomePage from "./pages/HomePage";
import TablePage from "./pages/TablePage";
import CreatePage from "./pages/CreatePage";
import UpdatePage from "./pages/UpdatePage";
import ChildrenPage from "./pages/ChildrenPage";

function App() {
  return (
    <div className="">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage/>} />
          <Route path="/table" element={<TablePage/>} />
          <Route path="/create" element={<CreatePage/>} />
          <Route path="/update/:productId" element={<UpdatePage/>} />
          <Route path="/children-props" element={<ChildrenPage/>} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
