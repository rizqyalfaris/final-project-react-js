import { Button } from "antd";
import React from "react";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <header class="shadow-lg py-3 bg-white px-12 sticky top-0 z-10">
      <nav class="mx-auto max-w-7xl flex justify-between">
        <div>
          <img src="https://logos.textgiraffe.com/logos/logo-name/Faris-designstyle-cartoon-m.png" class="w-28 h-16" alt=""></img>
        </div>
        <ul class="flex items-center gap-6 text-cyan-500 text-xl list-none">
          <li>
            <a href="/"> Home </a>
          </li>
          <li>
            <a href="/create"> Products </a>
          </li>
          <li>
            <a href="/table"> Table </a>
          </li>
        </ul>

        <div class="flex items-center gap-4">
          <a href="/register">
          <button type="button"
                    class="text-white bg-cyan-700 hover:bg-white focus:ring-4 focus:outline-none focus:ring-white font-medium rounded-lg text-sm px-4 py-2 text-center mr-3 md:mr-0 dark:bg-cyan-600 dark:hover:bg-cyan-700 dark:focus:ring-white">Register</button>
          </a>
          <a href="/login">
          <button type="button"
                    class="border border-2 border-cyan-600 text-cyan-600 bg-white hover:bg-white focus:ring-4 focus:outline-none focus:ring-white font-medium rounded-lg text-sm px-4 py-2 text-center mr-3 md:mr-1 dark:bg-white dark:hover:ring-white dark:focus:ring-white">Login</button>
          </a>
        </div>
      </nav>
    </header>
  );
}

export default Navbar;
