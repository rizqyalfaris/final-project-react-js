import React from 'react'

function Footer() {
  return (
    <div className='text-white text-center text-3xl bg-blue-600 py-5 my-4'>
        Copyright Muhammad Rizqy Al Faris
    </div>
  )
}

export default Footer