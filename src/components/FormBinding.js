import React, { useState } from 'react'

function FormBinding() {
    const [input, setInput] = useState({
        nama_barang: "",
        stock_barang: 0,
        harga_barang: 0,
        deskripsi: "", 
        checkbox: false,
        kategori: "",
        gambar_barang: "",
        deskripsi: ""

    });

    const handleChange = (event) => {

        // console.log(event.target.name);

        if (event.target.name === "nama_barang") {
            setInput({
                ...input,
                nama_barang: event.target.value,
            });
        } else if (event.target.name === "stock_barang") {
            setInput({
                ...input,
                stock_barang: event.target.value,
            });
        } else if (event.target.name === "harga_barang") {
            setInput({
                ...input,
                harga_barang: event.target.value,
            });
        } else if (event.target.name === "checkbox") {
            setInput({
                ...input,
                checkbox: event.target.checked,
            });
        } else if (event.target.name === "kategori") {
            setInput({
                ...input,
                kategori: event.target.value,
            });
        } else if (event.target.name === "gambar_barang") {
            setInput({
                ...input,
                gambar_barang: event.target.value,
            });
        } else if (event.target.name === "deskripsi") {
            setInput({
                ...input,
                deskripsi: event.target.value,
            });
        }
    };

    const handleSubmit = () => {
        console.log(input);
    };
    return (
        <div>
            <div
                className="mt-32 ml-20 mr-20 border-solid border-2 border-slate-50 rounded-lg shadow-lg dark:shadow-black/30 bg-white">
                <div className="pl-10 pt-7 pb-10 pr-10">
                    <p className="pb-7 font-bold text-lg text-cyan-700">Create Product</p>
                    <div className="flex gap-5">
                        <div className="mb-6 w-3/6">
                            <label for="nama_barang" className="block mb-2 text-sm font-medium">Nama Barang</label>
                            <input onChange={handleChange} name="nama_barang" type="text" id="nama_barang"
                                className="shadow-sm bg-white border border-white text-sm rounded-lg focus:ring-gray-300 focus:border-gray-300 block w-full p-2.5 dark:bg-white dark:border-gray-600 dark:placeholder-slate-400 dark:text-slate-800 dark:focus:ring-white dark:focus:border-white dark:shadow-sm-light"
                                placeholder="Masukkan harga barang" required></input>
                        </div>
                        <div className="mb-6 w-3/6">
                            <label for="stock_barang" className="block mb-2 text-sm font-medium">Stock Barang</label>
                            <input onChange={handleChange} name="stock_barang" type="text" id="stock_barang"
                                className="shadow-sm bg-white border border-white text-sm rounded-lg focus:ring-white focus:border-white block w-full p-2.5 dark:bg-white dark:border-gray-600 dark:placeholder-slate-400 dark:text-slate-800 dark:focus:ring-white dark:focus:border-white dark:shadow-sm-light"
                                placeholder="Masukkan stock barang" required></input>
                        </div>
                    </div>
                    <div className="flex gap-10">
                        <div className="mb-6 w-[455px]">
                            <label for="harga_barang" className="block mb-2 text-sm font-medium">Harga Barang</label>
                            <input onChange={handleChange} name="harga_barang" type="text" id="harga_barang"
                                className="shadow-sm bg-white border border-white text-sm rounded-lg focus:ring-white focus:border-white block w-full p-2.5 dark:bg-white dark:border-gray-600 dark:placeholder-slate-400 dark:text-slate-800 dark:focus:ring-white dark:focus:border-white dark:shadow-sm-light"
                                placeholder="Masukkan harga barang" required></input>
                        </div>
                        <div className="flex items-center">
                            <input onChange={handleChange} name="checkbox" type="checkbox"
                                className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"></input>
                            <label for="checked-checkbox"
                                className="ml-2 text-sm font-medium text-slate-800 dark:text-slate-800">Status Diskon</label>
                        </div>
                    </div>
                    <div className="flex gap-10">
                        <div className="mb-6 w-[455px]">
                            <label for="kategori" className="block mb-2 text-sm font-medium">Kategori</label>
                            <select onChange={handleChange} name="kategori" id="kategori"
                                className="shadow-sm bg-slate-800 border border-slate-800 text-sm rounded-lg focus:ring-slate-800 focus:border-slate-800 block w-full p-2.5 dark:bg-white dark:border-gray-600 dark:placeholder-slate-400 dark:text-slate-800 dark:focus:ring-slate-800 dark:focus:border-slate-800 dark:shadow-sm-light">
                                <option value="teknologi">Teknologi</option>
                                <option value="makanan">Makanan</option>
                                <option value="minuman">Minuman</option>
                                <option value="hiburan">Hiburan</option>
                                <option value="kendaraan">Kendaraan</option>
                            </select>
                        </div>
                        <div className="mb-6 w-4/6">
                            <label for="gambar_barang" className="block mb-2 text-sm font-medium">Gambar Barang</label>
                            <input onChange={handleChange} name="gambar_barang" type="text" id="gambar_barang"
                                className="shadow-sm bg-white border border-white text-sm rounded-lg focus:ring-white focus:border-white block w-full p-2.5 dark:bg-white dark:border-gray-600 dark:placeholder-slate-400 dark:text-slate-800 dark:focus:ring-white dark:focus:border-white dark:shadow-sm-light"
                                placeholder="Masukkan image url" required></input>
                        </div>
                    </div>
                    <div className="flex gap-10">
                        <div className="mb-6 h-full w-full">
                            <label for="deskripsi" className="block mb-2 text-sm font-medium">Deskripsi</label>
                            <textarea onChange={handleChange} name="deskripsi" id="message" rows="4"
                                className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-white dark:border-gray-600 dark:placeholder-gray-400 dark:text-slate-800 dark:focus:ring-blue-500 dark:focus:border-blue-500"></textarea>
                        </div>
                    </div>
                    <div className="flex gap-2 justify-end" >
                        <button className="bg-">
                            <a href="homes.html"><button type="submit"
                                className="border border-2 border-cyan-600 text-cyan-600 hover:bg-cyan-700 focus:ring-4 focus:outline-none focus:ring-cyan-700 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-white dark:hover:bg-white dark:focus:ring-cyan-700">Cancel</button>
                            </a>
                        </button>
                        <button onChange={handleSubmit} type="submit"
                            className="text-white bg-cyan-700 hover:bg-cyan-700 focus:ring-4 focus:outline-none focus:ring-cy font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-cyan-600 dark:hover:bg-cyan-600 dark:focus:ring-cyan-600">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default FormBinding