import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { GlobalContext } from "../context/GlobalContext";
import { useNavigate, useParams } from "react-router-dom";
import { Helmet } from "react-helmet";

function UpdateForm() {
  const { fetchProducts } = useContext(GlobalContext);
  const { productId } = useParams();
  const navigate = useNavigate();
  const [visible, setVisible] = useState(false);

  const [input, setInput] = useState({
    name: "",
    stock: 0,
    harga: 0,
    description: "",
    is_diskon: false,
    harga_diskon: 0,
    category: "",
    image_url: "",
  });

  const handleChange = (event) => {
    // console.log(event.target.name);

    if (event.target.name === "name") {
      setInput({
        ...input,
        name: event.target.value,
      });
    } else if (event.target.name === "stock") {
      setInput({
        ...input,
        stock: event.target.value,
      });
    } else if (event.target.name === "harga") {
      setInput({
        ...input,
        harga: event.target.value,
      });
    } else if (event.target.name === "is_diskon") {
      setInput({
        ...input,
        is_diskon: event.target.checked,
      });
    } else if (event.target.name === "category") {
      setInput({
        ...input,
        category: event.target.value,
      });
    } else if (event.target.name === "image_url") {
      setInput({
        ...input,
        image_url: event.target.value,
      });
    } else if (event.target.name === "description") {
      setInput({
        ...input,
        description: event.target.value,
      });
    } else if (event.target.name === "harga_diskon") {
      setInput({
        ...input,
        harga_diskon: event.target.value,
      });
    }
  };

  const handleSubmit = async () => {
    console.log("Kita menjalankan post request");
    try {
      const response = await axios.put(
        `https://api-project.amandemy.co.id/api/products/${productId}`,
        {
          name: input.name,
          harga: input.harga,
          stock: input.stock,
          image_url: input.image_url,
          is_diskon: input.is_diskon,
          harga_diskon: input.harga_diskon,
          category: input.category,
          description: input.description,
        }
      );
      // console.log(response.data.data); //berhasil mengakses data products
      fetchProducts();
      setInput({
        name: "",
        stock: 0,
        harga: 0,
        description: "",
        is_diskon: false,
        harga_diskon: 0,
        category: "",
        image_url: "",
      });
      alert("Product berhasil diubah");
      navigate("/table");
      //products sudah terisi data dari server
    } catch (error) {
      alert("Terjadi sesuatu error!");
    }
  };

  const fetchProductsDetail = async () => {
    try {
      const response = await axios.get(
        `https://api-project.amandemy.co.id/api/products/${productId}`
      );
      // console.log(response.data.data);
      const products = response.data.data;
      //products sudah terisi data dari server
      setInput({
        name: products.name,
        harga: products.harga,
        stock: products.stock,
        image_url: products.image_url,
        is_diskon: products.is_diskon,
        harga_diskon: products.harga_diskon,
        category: products.category,
        description: products.description,
      });
    } catch (error) {
      alert("Terjadi sesuatu error!");
    }
  };

  useEffect(() => {
    fetchProductsDetail();
  }, []);

  return (
    <div>
    <Helmet>
      <title>Update Product ID: {productId}</title>
    </Helmet>
      <div className="mt-12 ml-20 mr-20 border-solid border-2 border-slate-50 rounded-lg shadow-lg dark:shadow-black/30 bg-emerald-200">
        <div className="pl-10 pt-7 pb-10 pr-10">
          <p className="pb-7 font-bold text-lg text-cyan-700">Update Product</p>
          <div className="flex gap-5">
            <div className="mb-6 w-3/6">
              <label for="name" className="block mb-2 text-sm font-medium">
                Nama Barang
              </label>
              <input
                onChange={handleChange}
                name="name"
                type="text"
                id="name"
                className="shadow-sm bg-white border border-white text-sm rounded-lg focus:ring-gray-300 focus:border-gray-300 block w-full p-2.5 dark:bg-white dark:border-gray-600 dark:placeholder-slate-400 dark:text-slate-800 dark:focus:ring-white dark:focus:border-white dark:shadow-sm-light"
                placeholder="Masukkan harga barang"
                required
                value={input.name}
              ></input>
            </div>
            <div className="mb-6 w-3/6">
              <label for="stock" className="block mb-2 text-sm font-medium">
                Stock Barang
              </label>
              <input
                onChange={handleChange}
                name="stock"
                type="text"
                id="stock"
                className="shadow-sm bg-white border border-white text-sm rounded-lg focus:ring-white focus:border-white block w-full p-2.5 dark:bg-white dark:border-gray-600 dark:placeholder-slate-400 dark:text-slate-800 dark:focus:ring-white dark:focus:border-white dark:shadow-sm-light"
                placeholder="Masukkan stock barang"
                required
                value={input.stock}
              ></input>
            </div>
          </div>
          <div className="flex gap-10">
            <div className="mb-6 w-[455px]">
              <label for="harga" className="block mb-2 text-sm font-medium">
                Harga Barang
              </label>
              <input
                onChange={handleChange}
                name="harga"
                type="text"
                id="harga"
                className="shadow-sm bg-white border border-white text-sm rounded-lg focus:ring-white focus:border-white block w-full p-2.5 dark:bg-white dark:border-gray-600 dark:placeholder-slate-400 dark:text-slate-800 dark:focus:ring-white dark:focus:border-white dark:shadow-sm-light"
                placeholder="Masukkan harga barang"
                required
                value={input.harga}
              ></input>
            </div>
            <div className="flex items-center">
              <input
                onChange={handleChange}
                name="is_diskon"
                type="checkbox"
                className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                value={input.is_diskon}
                onClick={() => setVisible(!visible)}
              ></input>
              <label
                for="is_diskon"
                className="ml-2 text-sm font-medium text-slate-800 dark:text-slate-800"
              >
                Status Diskon
              </label>
            </div>
            {visible && (
              <div class="mb-6 w-3/6 pl-4">
                <label
                  for="harga_diskon"
                  class="block mb-2 text-sm font-medium"
                >
                  Harga Diskon
                </label>
                <input
                  onChange={handleChange}
                  name="harga_diskon"
                  type="text"
                  id="harga_diskon"
                  class="shadow-sm bg-white border border-white text-sm rounded-lg focus:ring-white focus:border-white block w-full p-2.5 dark:bg-white dark:border-gray-600 dark:placeholder-slate-400 dark:text-slate-800 dark:focus:ring-white dark:focus:border-white dark:shadow-sm-light"
                  placeholder="Masukkan harga diskon"
                  required
                  value={input.harga_diskon}
                ></input>
              </div>
            )}
          </div>
          <div className="flex gap-10">
            <div className="mb-6 w-[455px]">
              <label for="category" className="block mb-2 text-sm font-medium">
                Kategori
              </label>
              <select
                onChange={handleChange}
                name="category"
                id="category"
                className="shadow-sm bg-slate-800 border border-slate-800 text-sm rounded-lg focus:ring-slate-800 focus:border-slate-800 block w-full p-2.5 dark:bg-white dark:border-gray-600 dark:placeholder-slate-400 dark:text-slate-800 dark:focus:ring-slate-800 dark:focus:border-slate-800 dark:shadow-sm-light"
                value={input.category}
              >
                <option value="" disabled>
                  Pilih Kategori
                </option>
                <option value="teknologi">Teknologi</option>
                <option value="makanan">Makanan</option>
                <option value="minuman">Minuman</option>
                <option value="hiburan">Hiburan</option>
                <option value="kendaraan">Kendaraan</option>
              </select>
            </div>
            <div className="mb-6 w-4/6">
              <label for="image_url" className="block mb-2 text-sm font-medium">
                Gambar Barang
              </label>
              <input
                onChange={handleChange}
                name="image_url"
                type="text"
                id="image_url"
                className="shadow-sm bg-white border border-white text-sm rounded-lg focus:ring-white focus:border-white block w-full p-2.5 dark:bg-white dark:border-gray-600 dark:placeholder-slate-400 dark:text-slate-800 dark:focus:ring-white dark:focus:border-white dark:shadow-sm-light"
                placeholder="Masukkan image url"
                required
                value={input.image_url}
              ></input>
            </div>
          </div>
          <div className="flex gap-10">
            <div className="mb-6 h-full w-full">
              <label
                for="description"
                className="block mb-2 text-sm font-medium"
              >
                Deskripsi
              </label>
              <textarea
                onChange={handleChange}
                name="description"
                id="description"
                rows="4"
                className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-white dark:border-gray-600 dark:placeholder-gray-400 dark:text-slate-800 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                value={input.description}
              ></textarea>
            </div>
          </div>
          <div className="flex gap-2 justify-end">
            <button className="bg-">
              <a href="homes.html">
                <button
                  type="submit"
                  className="border border-2 border-cyan-600 text-cyan-600 hover:bg-cyan-700 focus:ring-4 focus:outline-none focus:ring-cyan-700 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-white dark:hover:bg-white dark:focus:ring-cyan-700"
                >
                  Cancel
                </button>
              </a>
            </button>
            <button
              onClick={handleSubmit}
              type="submit"
              className="text-white bg-cyan-700 hover:bg-cyan-700 focus:ring-4 focus:outline-none focus:ring-cy font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-cyan-600 dark:hover:bg-cyan-600 dark:focus:ring-cyan-600"
            >
              Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default UpdateForm;
