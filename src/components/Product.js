import React from "react";

function Product({ product }) {
  return (
    // <div className="mt-4 grid md: grid-cols-4 pb-10">
    //     <div className="border-solid border-2 border-sky-500 rounded-lg bg-white">
    //         <img src={product.image_url} className="rounded-lg h-80 w-96" />
    //         <label className="p-2">{product.name}</label><br />
    //         <label className="p-2 line-through">Rp {product.harga}</label><br />
    //         <label className="p-2 text-orange-500">Rp {product.harga_diskon}</label><br/>
    //         <label className="p-2 text-cyan-500">Stock {product.stock}</label>
    //     </div>
    // </div>
    <section class="my-10 mx-12">
      <div class="mx-auto max-w-7xl">
        <div class="grid grid-cols-4 gap-8 my-8">
          <a class="block h-full no-underline visited:text-inherit">
            <div class="h-full bg-white rounded-xl shadow-lg hover:shadow-2xl transition ease-in-out delay-75 hover:-translate-y-1 hover:scale-105 flex flex-col">
              <img
                src={product.image_url}
                class="w-full h-44 lg:h-64 object-cover rounded-t-xl"
                alt=""
              ></img>
              <div class="grow flex flex-col justify-between px-4 my-5 gap-4">
                <h1 class="text-gray-700 text-xl">{product.name}</h1>
                <div>
                  <h1 class="text-2xl font-normal">Rp {product.harga}</h1>
                </div>
                <p class="text-blue-500">Stock {product.stock}</p>
              </div>
            </div>
          </a>
          <a class="block h-full no-underline visited:text-inherit">
            <div class="h-full bg-white rounded-xl shadow-lg hover:shadow-2xl transition ease-in-out delay-75 hover:-translate-y-1 hover:scale-105 flex flex-col">
              <img
                src={product.image_url}
                class="w-full h-44 lg:h-64 object-cover rounded-t-xl"
                alt=""
              ></img>
              <div class="grow flex flex-col justify-between px-4 my-5 gap-4">
                <h1 class="text-gray-700 text-xl">{product.name}</h1>
                <div>
                  <h1 class="text-2xl font-normal">Rp {product.harga}</h1>
                </div>
                <p class="text-blue-500">Stock {product.stock}</p>
              </div>
            </div>
          </a>
          <a class="block h-full no-underline visited:text-inherit">
            <div class="h-full bg-white rounded-xl shadow-lg hover:shadow-2xl transition ease-in-out delay-75 hover:-translate-y-1 hover:scale-105 flex flex-col">
              <img
                src={product.image_url}
                class="w-full h-44 lg:h-64 object-cover rounded-t-xl"
                alt=""
              ></img>
              <div class="grow flex flex-col justify-between px-4 my-5 gap-4">
                <h1 class="text-gray-700 text-xl">{product.name}</h1>
                <div>
                  <h1 class="text-2xl font-normal">Rp {product.harga}</h1>
                </div>
                <p class="text-blue-500">Stock {product.stock}</p>
              </div>
            </div>
          </a>
          <a class="block h-full no-underline visited:text-inherit">
            <div class="h-full bg-white rounded-xl shadow-lg hover:shadow-2xl transition ease-in-out delay-75 hover:-translate-y-1 hover:scale-105 flex flex-col">
              <img
                src={product.image_url}
                class="w-full h-44 lg:h-64 object-cover rounded-t-xl"
                alt=""
              ></img>
              <div class="grow flex flex-col justify-between px-4 my-5 gap-4">
                <h1 class="text-gray-700 text-xl">{product.name}</h1>
                <div>
                  <h1 class="text-2xl font-normal">Rp {product.harga}</h1>
                </div>
                <p class="text-blue-500">Stock {product.stock}</p>
              </div>
            </div>
          </a>
        </div>
      </div>
    </section>
  );
}

export default Product;
