import axios from "axios";
import { useFormik } from "formik";
import React, { useContext, useState } from "react";
import { GlobalContext } from "../context/GlobalContext";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";

const validationSchema = Yup.object({
  name: Yup.string().required("Nama Barang wajib diisi"),
  stock: Yup.number().required("Stok Barang wajib diisi"),
  harga: Yup.number().required("Harga Barang wajib diisi"),
  description: Yup.string().required("Deskripsi wajib diisi"),
  is_diskon: Yup.boolean(),
  harga_diskon: Yup.number().required("Harga diskon wajib diisi"),
  category: Yup.string().required("Kategori wajib diisi"),
  image_url: Yup.string()
    .required("Gambar wajib diisi")
    .url("Format url tidak valid"),
});

function CreateForm() {
  const { fetchProducts } = useContext(GlobalContext);
  const navigate = useNavigate();

  const [visible, setVisible] = useState(false);

  const [input, setInput] = useState({
    name: "",
    stock: null,
    harga: null,
    description: "",
    is_diskon: false,
    harga_diskon: null,
    category: "",
    image_url: "",
  });

  const onSubmit = async (values) => {
    console.log(values);
    console.log("Kita menjalankan post request");
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/products",
        {
          name: values.name,
          harga: values.harga,
          stock: values.stock,
          image_url: values.image_url,
          is_diskon: values.is_diskon,
          harga_diskon: values.harga_diskon,
          category: values.category,
          description: values.description,
        }
      );
      // console.log(response.data.data); //berhasil mengakses data products
      fetchProducts();
      setInput({
        name: "",
        stock: 0,
        harga: 0,
        description: "",
        is_diskon: false,
        harga_diskon: 0,
        category: "",
        image_url: "",
      });
      alert("Product berhasil dibuat");
      navigate("/table");
      //products sudah terisi data dari server
    } catch (error) {
      alert("Terjadi sesuatu error!");
    }
  };

  const {
    handleChange,
    values,
    handleSubmit,
    errors,
    touched,
    handleBlur,
    setFieldValue,
    setFieldTouched,
  } = useFormik({
    initialValues: input,
    onSubmit: onSubmit,
    validationSchema: validationSchema,
  });

  console.log(touched);

  return (
    <div>
      <div className="mt-12 ml-20 mr-20 border-solid border-2 border-slate-50 rounded-lg shadow-lg dark:shadow-black/30 bg-emerald-200">
        <div className="pl-10 pt-7 pb-10 pr-10">
          <p className="pb-7 font-bold text-lg text-cyan-700">Create Product</p>
          <div className="flex gap-5">
            <div className="mb-6 w-3/6">
              <label for="name" className="block mb-2 text-sm font-medium">
                Nama Barang
              </label>
              <input
                onChange={handleChange}
                name="name"
                type="text"
                id="name"
                className="shadow-sm bg-white border border-white text-sm rounded-lg focus:ring-gray-300 focus:border-gray-300 block w-full p-2.5 dark:bg-white dark:border-gray-600 dark:placeholder-slate-400 dark:text-slate-800 dark:focus:ring-white dark:focus:border-white dark:shadow-sm-light"
                placeholder="Masukkan harga barang"
                required
                value={values.name}
                onBlur={handleBlur}
              ></input>
              {touched.name === true && errors.name != null && (
                <p className="text-red-500 text-lg">{errors.name}</p>
              )}
            </div>
            <div className="mb-6 w-3/6">
              <label for="stock" className="block mb-2 text-sm font-medium">
                Stock Barang
              </label>
              <input
                onChange={handleChange}
                name="stock"
                type="text"
                id="stock"
                className="shadow-sm bg-white border border-white text-sm rounded-lg focus:ring-white focus:border-white block w-full p-2.5 dark:bg-white dark:border-gray-600 dark:placeholder-slate-400 dark:text-slate-800 dark:focus:ring-white dark:focus:border-white dark:shadow-sm-light"
                placeholder="Masukkan stock barang"
                required
                value={values.stock}
                onBlur={handleBlur}
              ></input>
              {touched.stock === true && errors.stock != null && (
                <p className="text-red-500 text-lg">{errors.stock}</p>
              )}
            </div>
          </div>
          <div className="flex gap-10">
            <div className="mb-6 w-[455px]">
              <label for="harga" className="block mb-2 text-sm font-medium">
                Harga Barang
              </label>
              <input
                onChange={handleChange}
                name="harga"
                type="text"
                id="harga"
                className="shadow-sm bg-white border border-white text-sm rounded-lg focus:ring-white focus:border-white block w-full p-2.5 dark:bg-white dark:border-gray-600 dark:placeholder-slate-400 dark:text-slate-800 dark:focus:ring-white dark:focus:border-white dark:shadow-sm-light"
                placeholder="Masukkan harga barang"
                required
                value={values.harga}
                onBlur={handleBlur}
              ></input>
              {touched.harga === true && errors.harga != null && (
                <p className="text-red-500 text-lg">{errors.harga}</p>
              )}
            </div>
            <div className="flex items-center">
              <input
                onChange={(event) =>
                  setFieldValue("is_diskon", event.target.checked)
                }
                onBlur={() => setFieldTouched("is_diskon")}
                name="is_diskon"
                type="checkbox"
                className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                value={values.is_diskon}
                onClick={() => setVisible(!visible)}
              ></input>
              <label
                for="is_diskon"
                className="ml-2 text-sm font-medium text-slate-800 dark:text-slate-800"
              >
                Status Diskon
              </label>
              {touched.is_diskon === true && errors.is_diskon != null && (
                <p className="text-red-500 text-lg">{errors.is_diskon}</p>
              )}
            </div>
            {visible && (
              <div class="mb-6 w-3/6 pl-4">
                <label
                  for="harga_diskon"
                  class="block mb-2 text-sm font-medium"
                >
                  Harga Diskon
                </label>
                <input
                  onChange={handleChange}
                  name="harga_diskon"
                  type="text"
                  id="harga_diskon"
                  class="shadow-sm bg-white border border-white text-sm rounded-lg focus:ring-white focus:border-white block w-full p-2.5 dark:bg-white dark:border-gray-600 dark:placeholder-slate-400 dark:text-slate-800 dark:focus:ring-white dark:focus:border-white dark:shadow-sm-light"
                  placeholder="Masukkan harga diskon"
                  required
                  value={values.harga_diskon}
                  // onBlur={handleBlur}
                ></input>
                {/* { errors.harga_diskon != null && <p className="text-red-500 text-lg">{errors.harga_diskon}</p>} */}
              </div>
            )}
          </div>
          <div className="flex gap-10">
            <div className="mb-6 w-[455px]">
              <label for="category" className="block mb-2 text-sm font-medium">
                Kategori
              </label>
              <select
                onChange={(event) =>
                  setFieldValue("category", event.target.value)
                }
                onBlur={() => setFieldTouched("category")}
                name="category"
                id="category"
                className="shadow-sm bg-slate-800 border border-slate-800 text-sm rounded-lg focus:ring-slate-800 focus:border-slate-800 block w-full p-2.5 dark:bg-white dark:border-gray-600 dark:placeholder-slate-400 dark:text-slate-800 dark:focus:ring-slate-800 dark:focus:border-slate-800 dark:shadow-sm-light"
                value={values.category}
              >
                <option value="" disabled>
                  Pilih Kategori
                </option>
                <option value="teknologi">Teknologi</option>
                <option value="makanan">Makanan</option>
                <option value="minuman">Minuman</option>
                <option value="hiburan">Hiburan</option>
                <option value="kendaraan">Kendaraan</option>
              </select>
              {touched.category === true && errors.category != null && (
                <p className="text-red-500 text-lg">{errors.category}</p>
              )}
            </div>
            <div className="mb-6 w-4/6">
              <label for="image_url" className="block mb-2 text-sm font-medium">
                Gambar Barang
              </label>
              <input
                onChange={handleChange}
                name="image_url"
                type="text"
                id="image_url"
                className="shadow-sm bg-white border border-white text-sm rounded-lg focus:ring-white focus:border-white block w-full p-2.5 dark:bg-white dark:border-gray-600 dark:placeholder-slate-400 dark:text-slate-800 dark:focus:ring-white dark:focus:border-white dark:shadow-sm-light"
                placeholder="Masukkan image url"
                required
                value={values.image_url}
                onBlur={handleBlur}
              ></input>
              {touched.image_url === true && errors.image_url != null && (
                <p className="text-red-500 text-lg">{errors.image_url}</p>
              )}
            </div>
          </div>
          <div className="flex gap-10">
            <div className="mb-6 h-full w-full">
              <label
                for="description"
                className="block mb-2 text-sm font-medium"
              >
                Deskripsi
              </label>
              <textarea
                onChange={handleChange}
                name="description"
                id="description"
                rows="4"
                className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-white dark:border-gray-600 dark:placeholder-gray-400 dark:text-slate-800 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                value={values.description}
                onBlur={handleBlur}
              ></textarea>
              {touched.description === true && errors.description != null && (
                <p className="text-red-500 text-lg">{errors.description}</p>
              )}
            </div>
          </div>
          <div className="flex gap-2 justify-end">
            <button className="bg-">
              <a href="homes.html">
                <button
                  type="submit"
                  className="border border-2 border-cyan-600 text-cyan-600 hover:bg-cyan-700 focus:ring-4 focus:outline-none focus:ring-cyan-700 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-white dark:hover:bg-white dark:focus:ring-cyan-700"
                >
                  Cancel
                </button>
              </a>
            </button>
            <button
              onClick={handleSubmit}
              type="submit"
              className="text-white bg-cyan-700 hover:bg-cyan-700 focus:ring-4 focus:outline-none focus:ring-cy font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-cyan-600 dark:hover:bg-cyan-600 dark:focus:ring-cyan-600"
            >
              Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CreateForm;
